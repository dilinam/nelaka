import React from 'react';


export function Header(props) {
    return (
        <>
            <div className={"xo-header text-center"}>
                <div className={"xo-header__title text-center"}>
                    <h3>Knitting Department</h3>
                    <p>Machine Status</p>
                </div>
                <div className={"xo-header__icons"}>
                    <i className="bi bi-gear-fill"/>
                    <i className="bi bi-bell-fill"/>
                    <i className="bi bi-person-circle"/>
                </div>
            </div>
        </>
    )
}
