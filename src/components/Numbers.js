import React from "react";

const Numbers = (props) => {
    return (
        <>
            <div className={"xo-machine-big-numbers"}>
                <div>
                    <h2 className={"text-primary"}>100%</h2>
                    <p>some text</p>
                </div>
                <div>
                    <h2 className={"text-primary"}>100%</h2>
                    <p>some text</p>
                </div>
                <div>
                    <h2 className={"text-primary"}>100%</h2>
                    <p>some text</p>
                </div>
            </div>
        </>
    );
};

export default Numbers;
