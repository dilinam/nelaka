import React from 'react';
import {ArcElement, Chart as ChartJS, Tooltip} from 'chart.js';
import {Doughnut} from 'react-chartjs-2';

ChartJS.register(ArcElement, Tooltip);

export const data = {
    labels: ['cat1', 'cat2',],
    datasets: [
        {
            label: '# of Votes',
            data: [75, 25,],
            backgroundColor: [
                '#32a852',
                '#b3b3b3',
            ],
            borderWidth: 0,
        },
    ],
};

const options = {
    cutout: 40
};

export function DoughnutChart() {
    return <Doughnut type={"doughnut"} options={options} data={data}/>;
}
