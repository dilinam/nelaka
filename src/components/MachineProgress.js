import React from "react";
import {ProgressBar} from "react-bootstrap";

const MachineProgress = (props) => {
    return (
        <>
            <div className={"xo-machine-progress"}>
                <h6>progress title</h6>
                <ProgressBar variant="success" now={40} />
                <h6>progress title</h6>
                <ProgressBar variant="success" now={95} />
            </div>
        </>
    );
};

export default MachineProgress;
