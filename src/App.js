import './style/style.scss';
import {Route, Routes} from "react-router-dom";
import Machines from "./views/Machines";
import WeeklyPlan from './views/WeeklyPlan.js';
import {Nav} from "react-bootstrap";
import React from "react";
import MachineDetails from "./views/MachneDetails";
import teejayLogo from './img/teejay_logo.png'
import xoLogo from "./img/Logo Colour.webp"

function App() {
    return (
        <div className="App">
            <div className={"xo-main"}>
                <div className={"container-fluid"}>
                    <div className={"row"}>
                        <div className={"col-2"}>
                            <div className={"xo-nav text-center"}>
                                <Nav defaultActiveKey="/home" className="flex-column ">
                                    <div className={"xo-nav__logo"}>
                                        <img src={teejayLogo} alt="teejay"/>
                                    </div>
                                    <Nav.Link href="/">Machine Status</Nav.Link>
                                    <Nav.Link href="/">Group</Nav.Link>
                                    <Nav.Link href="/">Knitting Spec</Nav.Link>
                                    <Nav.Link href="/">Shift Roster</Nav.Link>
                                    <Nav.Link href="/weekly-plan">Weekly Plan</Nav.Link>
                                </Nav>
                                <div>
                                    <img src={xoLogo} alt="xo"/>
                                </div>
                            </div>
                        </div>
                        <div className={"col-10"}>
                            <div className={"xo-main__content"}>
                                <Routes>
                                    <Route path="/" element={<Machines/>}/>
                                    <Route path="weekly-plan" element={<WeeklyPlan/>}/>
                                    <Route path="details" element={<MachineDetails/>}/>
                                </Routes>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
