import React from "react";
import {Card, Tab, Tabs} from "react-bootstrap";
import {DoughnutChart} from "../components/DoughnutChart.js";
import {Link} from "react-router-dom";
import {Header} from "../components/Header";

const chartList = [];
let i;
for (i = 0; i < 10; i++) {
    chartList.push(
        <div className={"xo-machine-card col-12 col-xl-3 col-md-4 col-sm-6 "} key={i}>
            <Link to="/details">
                <Card>
                    <Card.Body>
                        <span className={"xo-tag"}> Active</span>
                        <p>STENTER 1</p>
                        <p>EPF number: 1</p>
                        <p>Efficiency:80%</p>
                        <div className={"xo-machine-card__chart"}>
                            <DoughnutChart/>
                        </div>
                        <p>Batch:0000</p>
                        <p>Quality:abcd</p>
                        <p>Length:50</p>
                    </Card.Body>
                </Card>
            </Link>
        </div>
    );
}
;

const Machines = (props) => {
    return (
        <>
            <div className={"xo-header-wrapper position-relative"}>
                <Header />
            </div>
            <div className={"xo-machines"}>
                <Tabs defaultActiveKey="category1" id="uncontrolled-tab-example" className="mb-3">
                    <Tab eventKey="category1" title="Category 1">
                        <div className="row xo-tab-content">
                            {chartList}
                        </div>
                    </Tab>
                    <Tab eventKey="category2" title="Category 2">
                    </Tab>
                </Tabs>
            </div>
        </>
    );
};

export default Machines;
