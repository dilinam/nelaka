import React from "react";
import {Header} from "../components/Header";
import {Tab, Tabs} from "react-bootstrap";
import {MachineMetaData} from "../components/MachineMetaData";
import {LineChart} from "../components/LineChart";
import Numbers from "../components/Numbers";
import MachineProgress from "../components/MachineProgress";

const MachineDetails = (props) => {
    return (
        <>
            <div className={"xo-header-wrapper position-relative"}>
                <Header/>
            </div>
            <div className={"xo-machines"}>
                <Tabs defaultActiveKey="category1" id="uncontrolled-tab-example" className="mb-3">
                    <Tab eventKey="category1" title="Category 1">
                        <div className="row xo-tab-content">
                            <div className={"row"}>
                                <div className="col-12 col-xl-3 col-md-4 col-sm-6">
                                    <MachineMetaData/>
                                    <LineChart/>
                                </div>
                                <div className="col-12 col-xl-3 col-md-4 col-sm-6">
                                    <Numbers/>
                                    <MachineProgress/>
                                    <LineChart/>
                                </div>
                                <div className="col-12 col-xl-3 col-md-4 col-sm-6">
                                    <MachineMetaData/>
                                    <MachineMetaData/>
                                </div>
                                <div className="col-12 col-xl-3 col-md-4 col-sm-6">
                                    <MachineMetaData/>
                                    <MachineMetaData/>
                                </div>
                            </div>
                        </div>
                    </Tab>
                    <Tab eventKey="category2" title="Category 2">
                    </Tab>
                </Tabs>
            </div>
        </>
    );
};

export default MachineDetails;
